
/*
 * One Click URPMI
 * This tool allow to users easy add Mageia repositories
 * http://urpmi.mageialinux.ru
 * https://bitbucket.org/xxblx/oneclick-urpmi
 * GNU GPL v3
 * Oleg xxblx; Mageia Russian Community
 * 2014 - 2015
 * 
 * One Click URPMI uses few additional JavaScript libraries: 
 * 1. jQuery (MIT License)
 * http://jquery.com/
 * 
 * 2. FileSaver.js (MIT License)
 * Used for file generation on client-side
 * https://github.com/eligrey/FileSaver.js
 */

var lng = ""; // language

var rep_dict = {}; // repositories
rep_dict["mrc"] = "http://packages.mageialinux.ru/mageia";
rep_dict["yamrc"] = "http://mirror.yandex.ru/mirrors/packages.mageialinux.ru/mageia";
rep_dict["blogdrake"] = "http://ftp.blogdrake.net/mageia/mageia";
rep_dict["tarakbumba"] = "http://tarakbumba.mageia.org.pl/";
rep_dict["mlo"] = "ftp://download.asso-linux-online.fr/download/packages-mlo/Mageia/";
rep_dict["pol"] = "http://repo.mageia.org.pl/";
rep_dict["unrealnetworks"] = "http://mageia.unrealnetworks.co.uk/mga";
rep_dict["cyron"] = "http://www.cyron.cz/rpm/mageia/";

var def_branches = ["core", "nonfree", "tainted"]; 

// Add keys for proxy using
function proxy_keys()
{
    // If proxy checkbox is checked
    if (document.getElementById("proxy_cb").checked) {
        
        var data = "";
        // Get values from <input>s with host, user and password
        var host = document.getElementById("phost").value;
        var user = document.getElementById("puser").value;
        var pswd = document.getElementById("ppswd").value;
        
        // Add exist values to data
        if (host) {  // host
            data += "--proxy \n" + document.getElementById("phost").value + " \n";
        }
        
        if (user) {  // user
            data += "--proxy-user \n" + user;
            
            if (pswd) {  // password
                data += ":" + pswd + " \n";
            } else {
                data += " \n";
            }
        }
        
        return data;
        
    } else {
        return "";
    }
}

// Add default mirror from mirrorlist based on user's geoip
function add_repo_geoip(r, a)
{
    // r - release number
    // a - arch
    var data = proxy_keys();  // --proxy and --proxy-user
    data += "--distrib \n--mirrorlist \nhttp://mirrors.mageia.org/api/mageia." + r + "." + a + ".list";
    var file = new Blob([data], {type: "application/x-urpmi-media;charset=utf-8"});
    // Save "file" with "data" as oneclickurpmi.urpmi-media
    saveAs(file, "oneclickurpmi.urpmi-media");
}

// Add repository
function add_repo(url, r, a, min_r, distrib, pubkey, 
                  rep_name, noarch, save_file, branch)
// url - url to repo, r -relase, a - arch, 
// min_r - minimal release number for this repo
// noarch - ignore arch and add noarch
// save_file - save file or just return generated data
// branch - add branch to url after arch
{
    // Default values
    distrib = typeof distrib != "undefined" ? distrib: true;
    pubkey = typeof pubkey != "undefined" ? pubkey: true;
    noarch = typeof noarch != "undefined" ? noarch: false;
    save_file = typeof save_file != "undefined" ? save_file: true;
    
    // Check release
    if (+r < min_r) {
        alert(tr[lng]["rep_support"] + min_r + tr[lng]["or_newer"]);
        return;
    }
    
    var data = proxy_keys();  // --proxy and --proxy-user
    
    if (distrib) {
        data += "--distrib \n";
    }
    
    if (!pubkey) {
        data += "--nopubkey \n";
    }
    
    if (rep_name) {  // repo's name
        data += rep_name + " \n";
    }
    
    // Add url
    if (branch) {
        data += url + r + "/" + a + "/" + branch + "/";
    } else if (noarch) {
        data += url + r + "/noarch/";
    } else {
        data += url + r + "/" + a + "/";
    }
    
    // Save file or return data
    if (save_file) {
        var file = new Blob([data], {type: "application/x-urpmi-media;charset=utf-8"});
        saveAs(file, "oneclickurpmi.urpmi-media");
    } else {
        return data;
    }
}

// Add repository with additional noarch
function add_repo_noarch(url, r, a, min_r, rep_name)
{
    // Check release
    if (+r < min_r) {
        alert(tr[lng]["rep_support"] + min_r + tr[lng]["or_newer"]);
        return;
    }
    
    var data = "";
    var n = rep_name;
    var n_noarch = n + "-noarch";
    
    // url, rel, arch, min_rel, distrib, pubkey, rep_name, noarch, save_file
    data += add_repo(url, r, a, min_r, false, true, n, false, false);
    data += "\n";
    data += add_repo(url, r, a, min_r, false, true, n_noarch, true, false);
                     
    // Save file
    var file = new Blob([data], {type: "application/x-urpmi-media;charset=utf-8"});
    saveAs(file, "oneclickurpmi.urpmi-media");
}

// Add repositories with few branches
function add_repo_branches(url, r, a, min_r, rep_name, branch_lst)
// branch_list - array with names of branches
{
    // Check release
    if (+r < min_r) {
        alert(tr[lng]["rep_support"] + min_r + tr[lng]["or_newer"]);
        return;
    }
    
    var data = "";
    
    for (var i = 0; i < branch_lst.length; i++) {
        
        // Repo's name
        var n = rep_name;
        
        // If branch name not empty
        if (branch_lst[i]) {
            n += "-" + branch_lst[i];
        }
        
        // Get data
        // url, rel, arch, min_rel, distrib, pubkey, rep_name, noarch, save_file, branch
        data += add_repo(url, r, a, min_r, false, true, n, false, false, branch_lst[i]);
        
        // If line not last, add "\n"
        if (i != (branch_lst.length - 1)) {
            data += "\n";
        }
    }
    
    // Save file
    var file = new Blob([data], {type: "application/x-urpmi-media;charset=utf-8"});
    saveAs(file, "oneclickurpmi.urpmi-media");
}

// Update translation of all elements on page. Language set by browser language
function update_language()
{
    // Get html collections of tags with text
    var label_tags = document.getElementsByTagName("label");
    var p_tags = document.getElementsByTagName("p");
    var legend_tags = document.getElementsByTagName("legend");
    var input_tags = document.getElementsByTagName("input");
    
    // Reload every element's text in every collection
    // On input - value, on all other - innerHTML
    // <label>
    for (var i = 0; i < label_tags.length; i++) {
        if (label_tags[i].id) {
            document.getElementById(label_tags[i].id).innerHTML = tr[lng][label_tags[i].id];
        }
    }
    
    // <p>
    for (var i = 0; i < p_tags.length; i++) {
        if (p_tags[i].id) {
            document.getElementById(p_tags[i].id).innerHTML = tr[lng][p_tags[i].id];
        }
    }
    // <legend>
    for (var i = 0; i < legend_tags.length; i++) {
        if (legend_tags[i].id) {
            document.getElementById(legend_tags[i].id).innerHTML = tr[lng][legend_tags[i].id];
        }
    }
    // <input>
    // Exceptions: this ids doesn't need update
    var input_excepts = {"phost": null, "puser": null, "ppswd": null};
    for (var i = 0; i < input_tags.length; i++) {
        if (input_tags[i].id && !(input_tags[i].id in input_excepts)) {
            document.getElementById(input_tags[i].id).value = tr[lng][input_tags[i].id];
        }
    }
}

// Web-browser detect
function detect_browser() 
{
    var _ff = false;
    var _blob = undefined;
    
    // Get browser user agent
    var user_agent = navigator.userAgent;
    
    // Check if firefox
    if (user_agent.match(/firefox/i)) {
        
        _ff = true;
        
        // Get an array with all numbers in user_agent string
        var n = (user_agent.match(/\d+/gi));
        
        // Firefox version < (older) 20 don't support blob interface
        // and FileSaver.js not works with them. Last 2 numbers are 
        // version like 24.2. we need compare floor (24) with 20.
        if (Number(n[n.length - 2]) < 20) {
            _blob = false;
        } else {
            _blob = true;
        }
    }
    
    return {ff: _ff, blob: _blob};
}

// Select page language from <select id="language">
function language_select(l)
// l - language.value from <select id="language">
{
    lng = l;
    update_language();
}

// Make div with manual mirror select
function manual_mirror_div() 
{
    for (i in servs) {
        
        if (servs[i].rels.length > 0) {
            
            // div with other elements
            var dv = document.createElement("div");
            
            // div with description
            var desc = document.createElement('div');
            desc.innerHTML = '<p class=\"headline\" style=\"margin-top:\
                              9px;\">' + servs[i].name + '</p> \
                              <p class="desc">' + servs[i].loc + "</p>";
            
            // div with buttons
            var btns = document.createElement('div');
            
            // HTTP & FTP
            if (servs[i].urls.ftp && servs[i].urls.http) {

                btns.innerHTML = '<input type=\"button\" class=\"button\"\
                                  value=\"HTTP\" onclick=\"\
                                  add_repo(servs[' + i + '].urls.http, release.value,\
                                  arch.value, ' + servs[i].rels[0] + ')\" /> \
                                  \
                                  <input type=\"button\" class=\"button\"\
                                  value=\"FTP\" onclick=\"\
                                  add_repo(servs[' + i + '].urls.ftp, release.value,\
                                  arch.value, ' + servs[i].rels[0] + ')\" />';
            // HTTP
            } else if (servs[i].urls.http) {
                btns.innerHTML = '<input type=\"button\" class=\"button\"\
                                  value=\"HTTP\" onclick=\"\
                                  add_repo(servs[' + i + '].urls.http, release.value,\
                                  arch.value, ' + servs[i].rels[0] + ')\" />';
            // FTP
            } else if (servs[i].urls.ftp) {
                btns.innerHTML = '<input type=\"button\" class=\"button\"\
                                  value=\"FTP\" onclick=\"\
                                  add_repo(servs[' + i + '].urls.ftp, release.value,\
                                  arch.value, ' + servs[i].rels[0] + ')\" />';
            }

            dv.appendChild(desc);
            dv.appendChild(btns);
            document.getElementById("spoiler_content").appendChild(dv);
        }
    }
}

// --- Running --- //
// Check browser language and set lng
if (navigator.language == "ru" || navigator.language == "ru-RU" 
    || navigator.language == "ru_RU") {
    lng = "ru";
} else if (navigator.language == "tr" || navigator.language == "tr-TR" 
    || navigator.language == "tr_TR") {
    lng = "tr";
} else if (navigator.language == "pl" || navigator.language == "pl-PL" 
    || navigator.language == "pl_PL") {
    lng = "pl";
} else {
    lng = "en";
}

var web_browser = detect_browser();
