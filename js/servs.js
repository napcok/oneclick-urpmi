// Content of this file automatically genereted by script.py

var servs = [];  // servers list

servs[0] = {name: "distrib-coffee.ipsl.jussieu.fr", loc: "Europe, France", urls: {ftp: "ftp://distrib-coffee.ipsl.jussieu.fr/pub/linux/Mageia/distrib/", http: "http://distrib-coffee.ipsl.jussieu.fr/pub/linux/Mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[1] = {name: "mageia.supp.name", loc: "Europe, Czech Republic", urls: {http: "http://mageia.supp.name/distrib/", }, rels: []};

servs[2] = {name: "distro.ibiblio.org", loc: "North America, United States of America", urls: {ftp: "ftp://distro.ibiblio.org/pub/linux/distributions/mageia/distrib/", http: "http://distro.ibiblio.org/pub/linux/distributions/mageia/distrib/", }, rels: [4, ]};

servs[3] = {name: "ftp-stud.hs-esslingen.de", loc: "Europe, Germany", urls: {ftp: "ftp://ftp-stud.hs-esslingen.de/pub/Mirrors/Mageia/distrib/", http: "http://ftp-stud.hs-esslingen.de/pub/Mirrors/Mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[4] = {name: "www.mirrorservice.org", loc: "Europe, United Kingdom", urls: {ftp: "ftp://www.mirrorservice.org/sites/mageia.org/pub/mageia/distrib/", http: "http://www.mirrorservice.org/sites/mageia.org/pub/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[5] = {name: "ftp.yzu.edu.tw", loc: "Asia, Taiwan", urls: {ftp: "ftp://ftp.yzu.edu.tw/Linux/Mageia/distrib/", http: "http://ftp.yzu.edu.tw/Linux/Mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[6] = {name: "ftp.cc.uoc.gr", loc: "Europe, Greece", urls: {ftp: "ftp://ftp.cc.uoc.gr/mirrors/linux/mageia/distrib/", http: "http://ftp.cc.uoc.gr/mirrors/linux/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[7] = {name: "mirror.aarnet.edu.au", loc: "Oceania, Australia", urls: {http: "http://mirror.aarnet.edu.au/pub/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[8] = {name: "aglae.biomedicale.univ-paris5.fr", loc: "Europe, France", urls: {http: "http://aglae.biomedicale.univ-paris5.fr/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[9] = {name: "mirror.internode.on.net", loc: "Oceania, Australia", urls: {http: "http://mirror.internode.on.net/pub/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[10] = {name: "free.nchc.org.tw", loc: "Asia, Taiwan", urls: {ftp: "ftp://free.nchc.org.tw/mageia/distrib/", http: "http://free.nchc.org.tw/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[11] = {name: "mirror.easthsia.com", loc: "North America, United States of America", urls: {http: "http://mirror.easthsia.com/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[12] = {name: "mirrors-usa.go-parts.com", loc: "North America, United States of America", urls: {ftp: "ftp://mirrors-usa.go-parts.com/mageia/distrib/", http: "http://mirrors-usa.go-parts.com/mageia/distrib/", }, rels: [4, ]};

servs[13] = {name: "ftp.twaren.net", loc: "Asia, Taiwan", urls: {ftp: "ftp://ftp.twaren.net/Linux/Mageia/distrib/", http: "http://ftp.twaren.net/Linux/Mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[14] = {name: "ftp.fi.muni.cz", loc: "Europe, Czech Republic", urls: {ftp: "ftp://ftp.fi.muni.cz/pub/linux/mageia/distrib/", http: "http://ftp.fi.muni.cz/pub/linux/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[15] = {name: "mirrors.ustc.edu.cn", loc: "Asia, China", urls: {ftp: "ftp://mirrors.ustc.edu.cn/mageia/distrib/", http: "https://mirrors.ustc.edu.cn/mageia/distrib/", }, rels: [3, 4, ]};

servs[16] = {name: "ftp5.gwdg.de", loc: "Europe, Germany", urls: {ftp: "ftp://ftp5.gwdg.de/pub/linux/mageia/distrib/", http: "http://ftp5.gwdg.de/pub/linux/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[17] = {name: "mageia.c3sl.ufpr.br", loc: "South America, Brazil", urls: {ftp: "ftp://mageia.c3sl.ufpr.br/mageia/distrib/", http: "http://mageia.c3sl.ufpr.br/distrib/", }, rels: [3, 4, ]};

servs[18] = {name: "mirror.cedia.org.ec", loc: "South America, Ecuador", urls: {ftp: "ftp://mirror.cedia.org.ec/mageia/distrib/", http: "http://mirror.cedia.org.ec/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[19] = {name: "ftp.icm.edu.pl", loc: "Europe, Poland", urls: {ftp: "ftp://ftp.icm.edu.pl/pub/Linux/sunsite/distributions/mageia/distrib/", http: "http://ftp.icm.edu.pl/pub/Linux/sunsite/distributions/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[20] = {name: "ftp.sunet.se", loc: "Europe, Sweden", urls: {ftp: "ftp://ftp.sunet.se/pub/Linux/distributions/mageia/distrib/", http: "http://ftp.sunet.se/pub/Linux/distributions/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[21] = {name: "mirrors.kernel.org", loc: "North America, United States of America", urls: {http: "http://mirrors.kernel.org/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[22] = {name: "ftp.itb.ac.id", loc: "Asia, Indonesia", urls: {ftp: "ftp://ftp.itb.ac.id/pub/mageia/distrib/", http: "http://ftp.itb.ac.id/pub/mageia/distrib/", }, rels: [1, 2, ]};

servs[23] = {name: "mirror.datacenter.by", loc: "Europe, Belarus", urls: {ftp: "ftp://mirror.datacenter.by/pub/mirrors/mageia.org/distrib/", http: "http://mirror.datacenter.by/pub/mirrors/mageia.org/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[24] = {name: "mirror.yandex.ru", loc: "Europe, Russian Federation", urls: {ftp: "ftp://mirror.yandex.ru/mageia/distrib/", http: "http://mirror.yandex.ru/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[25] = {name: "mageia.webconquest.com", loc: "North America, Canada", urls: {ftp: "ftp://mageia.webconquest.com/distrib/", http: "http://mageia.webconquest.com/distrib/", }, rels: [3, 4, ]};

servs[26] = {name: "fr2.rpmfind.net", loc: "Europe, France", urls: {ftp: "ftp://fr2.rpmfind.net/linux/mageia/distrib/", http: "http://fr2.rpmfind.net/linux/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[27] = {name: "ftp.sun.ac.za", loc: "Africa, South Africa", urls: {ftp: "ftp://ftp.sun.ac.za/pub/mirrors/mageia/distrib/", http: "http://ftp.sun.ac.za/ftp/pub/mirrors/mageia/distrib/", }, rels: [3, 4, ]};

servs[28] = {name: "mageia.unige.ch", loc: "Europe, Switzerland", urls: {http: "http://mageia.unige.ch/mirror/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[29] = {name: "mirror.dacentec.com", loc: "North America, United States of America", urls: {http: "http://mirror.dacentec.com/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[30] = {name: "ftp.tsukuba.wide.ad.jp", loc: "Asia, Japan", urls: {http: "http://ftp.tsukuba.wide.ad.jp/Linux/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[31] = {name: "ftp.snt.utwente.nl", loc: "Europe, Netherlands", urls: {ftp: "ftp://ftp.snt.utwente.nl/pub/os/linux/mageia/distrib/", http: "http://ftp.snt.utwente.nl/pub/os/linux/mageia/distrib/", }, rels: [3, 4, ]};

servs[32] = {name: "ftp.tku.edu.tw", loc: "Asia, Taiwan", urls: {ftp: "ftp://ftp.tku.edu.tw/Linux/Mageia/distrib/", http: "http://ftp.tku.edu.tw/Linux/Mageia/distrib/", }, rels: [3, 4, ]};

servs[33] = {name: "mageia.xfree.com.ar", loc: "South America, Argentina", urls: {http: "http://mageia.xfree.com.ar/distrib/", }, rels: [3, 4, ]};

servs[34] = {name: "mirror.tuxinator.org", loc: "Europe, Germany", urls: {ftp: "ftp://mirror.tuxinator.org/mageia/distrib/", http: "http://mirror.tuxinator.org/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[35] = {name: "ftp.belnet.be", loc: "Europe, Belgium", urls: {ftp: "ftp://ftp.belnet.be/mirror/mageia/distrib/", http: "http://ftp.belnet.be/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[36] = {name: "mirror.netcologne.de", loc: "Europe, Germany", urls: {ftp: "ftp://mirror.netcologne.de/mageia/distrib/", http: "http://mirror.netcologne.de/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[37] = {name: "mirror2.tuxinator.org", loc: "Europe, Germany", urls: {ftp: "ftp://mirror2.tuxinator.org/mageia/distrib/", http: "http://mirror2.tuxinator.org/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[38] = {name: "ftp.linux.org.tr", loc: "Asia, Turkey", urls: {ftp: "ftp://ftp.linux.org.tr/mageia/distrib/", http: "http://ftp.linux.org.tr/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[39] = {name: "ftp.las.ic.unicamp.br", loc: "South America, Brazil", urls: {ftp: "ftp://ftp.las.ic.unicamp.br/pub/mageia/distrib/", http: "http://ftp.las.ic.unicamp.br/pub/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[40] = {name: "mageia.mirror.garr.it", loc: "Europe, Italy", urls: {ftp: "ftp://mageia.mirror.garr.it/mirrors/mageia/distrib/", http: "http://mageia.mirror.garr.it/mirrors/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[41] = {name: "mirror.nexcess.net", loc: "North America, United States of America", urls: {ftp: "ftp://mirror.nexcess.net/mageia/distrib/", http: "http://mirror.nexcess.net/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[42] = {name: "linuxfreedom.com", loc: "North America, United States of America", urls: {http: "http://linuxfreedom.com/mageia/distrib/", }, rels: []};

servs[43] = {name: "ftp.nluug.nl", loc: "Europe, Netherlands", urls: {ftp: "ftp://ftp.nluug.nl/pub/os/Linux/distr/mageia/distrib/", http: "http://ftp.nluug.nl/pub/os/Linux/distr/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[44] = {name: "mirrors.yun-idc.com", loc: "Asia, China", urls: {ftp: "ftp://mirrors.yun-idc.com/mageia/distrib/", http: "http://mirrors.yun-idc.com/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[45] = {name: "vodka.sublink.org", loc: "Europe, Italy", urls: {ftp: "ftp://vodka.sublink.org/mageia/distrib/", http: "http://vodka.sublink.org/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[46] = {name: "mageia.fis.unb.br", loc: "South America, Brazil", urls: {http: "http://mageia.fis.unb.br/distrib/", }, rels: [4, ]};

servs[47] = {name: "ftp.uni-erlangen.de", loc: "Europe, Germany", urls: {ftp: "ftp://ftp.uni-erlangen.de/mirrors/Mageia/distrib/", http: "https://ftp.uni-erlangen.de/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[48] = {name: "ftp.acc.umu.se", loc: "Europe, Sweden", urls: {ftp: "ftp://ftp.acc.umu.se/mirror/mageia/distrib/", http: "http://ftp.acc.umu.se/mirror/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[49] = {name: "srv3.tuxinator.org", loc: "Europe, Germany", urls: {ftp: "ftp://srv3.tuxinator.org/mageia/distrib/", http: "http://srv3.tuxinator.org/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[50] = {name: "mirror.oss.maxcdn.com", loc: "Europe, Czech Republic", urls: {ftp: "ftp://mirror.oss.maxcdn.com/mageia/distrib/", http: "http://mirror.oss.maxcdn.com/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[51] = {name: "mirrors-ru.go-parts.com", loc: "Europe, Russian Federation", urls: {http: "http://mirrors-ru.go-parts.com/mageia/distrib/", }, rels: [4, ]};

servs[52] = {name: "mirrors-uk2.go-parts.com", loc: "Europe, United Kingdom", urls: {http: "http://mirrors-uk2.go-parts.com/mageia/distrib/", }, rels: [4, ]};

servs[53] = {name: "srv4.tuxinator.org", loc: "Europe, Germany", urls: {ftp: "ftp://srv4.tuxinator.org/mageia/distrib/", http: "https://srv4.tuxinator.org/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

servs[54] = {name: "mageia.jameswhitby.net", loc: "North America, United States of America", urls: {ftp: "ftp://mageia.jameswhitby.net/mageia/distrib/", http: "http://mageia.jameswhitby.net/mageia/distrib/", }, rels: [3, 4, ]};

servs[55] = {name: "mirror.rise.ph", loc: "Asia, Philippines", urls: {ftp: "ftp://mirror.rise.ph/mageia/distrib/", http: "http://mirror.rise.ph/mageia/distrib/", }, rels: [1, 2, 3, 4, ]};

