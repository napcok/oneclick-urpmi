
var tr = {}; // hash-table with translations

// key == element's id 
tr.ru = { // russian translation
    mga_rel: "Версия Mageia:",
    mga_arch: "Архитектура:",
    of_rep: "Официальные репозитории Mageia",
    def_mirror: "Зеркало по умолчанию",
    def_mirror_desc: "Зеркало будет выбрано автоматически, основываясь на вашем местоположении.",
    mrc_rep: "Репозиторий MRC",
    mrc_rep_desc: "Репозиторий русскоязычного сообщества пользователей Mageia.",
    yamrc_rep_desc: "Если наш сервер окажется недоступен по какой-то причине, вы можете воспользоваться Яндекс зеркалом.",
    other_repos: "Другие неофициальные репозитории",
    blogdrake_title: "Репозиторий BlogDRAKE", 
    blogdrake_desc: "<a href=\"http://blogdrake.net/\">BlogDRAKE</a> - Испанское сообщество",
    add_btn_blogdrake: "Добавить",
    spoiler_btn: "Спойлер", 
    spoiler_btn2: "Спойлер", 
    add_btn: "Добавить",
    add_btn_mrc: "Добавить",
    add_btn_yamrc: "Добавить зеркало",
    instr: "Инструкция",
    recbr: "Рекомендуемый веб-браузер - Firefox 20+",
    press_add: "1. Нажмите Добавить у нужного репозитория",
    open: "2. Открыть в - Добавить источник urpmi - ОК",
    auth: "3. Введите root-пароль - Аутентификация",
    press_yes: "4. Нажмите Да",
    ocu: "OneClick URPMI распространяется по лицензии GNU GPL v3 или более новой. Исходный код доступен <a href=\"https://bitbucket.org/xxblx/oneclick-urpmi\">на bitbucket.org</a>.",
    author: "<a href=\"http://xxblx.bitbucket.org/\">Oleg Kozlov (xxblx)</a> | <a href=\"http://forum.mageialinux.ru\" alt=\"MRC forums\">Mageia Russian Community</a> | 2014 - 2015",
    rep_support: "Данный репозиторий поддерживает Mageia ",
    or_newer: " или более новые выпуски",
    tarakbumba_title: "Репозиторий tarakbumba",
    tarakbumba_desc: "В этом <a href=\"http://tarakbumba.mageia.org.pl/\">репозитории</a> вы найдете rpm пакеты собранные и поддерживаемые майнтайнером <a href=\"http://tarakbumba.com/\">tarakbumba</a>.",
    add_btn_tarakbumba: "Добавить",
    manual_mirror: "Выбрать зеркало самостоятельно",
    scroll_btn: "Вверх",
    proxy_label: "Прокси",
    phost_label: "Адрес(:Порт)",
    puser_label: "Пользователь",
    ppswd_label: "Пароль",
    unrealnetworks_title: "Репозиторий UnrealNetworks.co.uk",
    unrealnetworks_desc: "Репозиторий пользователей <b>pete910</b> и <b>doctor5000</b> с <a href=\"https://forums.mageia.org\">forums.mageia.org</a>.",
    add_btn_unrealnetworks: "Добавить",
    pol_title: "Репозиторий Mageia.org.pl",
    pol_desc: "Репозиторий <a href=\"http://mageia.org.pl/\">Польского сообщества Mageia</a>.",
    add_btn_pol: "Добавить",
    cyron_title: "Репозиторий Cyron",
    cyron_desc: "<a href=\"http://cyron.cz/\">Cyron</a> - чешский сборщик пакетов.",
    add_btn_cyron: "Добавить",
    mol_title: "Репозиторий MageiaLinuxOnline",
    mol_desc: "<a href=\"http://www.mageialinux-online.org/\">MageiaLinuxOnline</a> - Французское сообщество Mageia.",
    add_btn_mol: "Добавить"
};

tr.en = { // english translation
    mga_rel: "Mageia release:",
    mga_arch: "Arch:",
    of_rep: "Mageia official repositories",
    def_mirror: "Default mirror",
    def_mirror_desc: "Mirror will be select automatically based on your geographical location.",
    mrc_rep: "MRC Repository",
    mrc_rep_desc: "Mageia Russian Community repository.",
    yamrc_rep_desc: "If our server will be not available you can use Yandex mirror.",
    other_repos: "Other unofficial repositories",
    blogdrake_title: "BlogDRAKE repository",
    blogdrake_desc: "<a href=\"http://blogdrake.net/\">BlogDRAKE</a> is a Spanish community",
    add_btn_blogdrake: "Add",
    spoiler_btn: "Spoiler", 
    spoiler_btn2: "Spoiler", 
    add_btn: "Add",
    add_btn_mrc: "Add",
    add_btn_yamrc: "Add mirror",
    instr: "Instruction",
    recbr: "Recommended web-browser - Firefox 20+",
    press_add: "1. Press Add Button at repository block",
    open: "2. Open - Add urpmi media - OK",
    auth: "3. Enter root password - Authentication",
    press_yes: "4. Press Yes",
    ocu: "OneClick URPMI licensed under GNU GPL v3 or newer. Source code available <a href=\"https://bitbucket.org/xxblx/oneclick-urpmi\">on bitbucket.org</a>.",
    author: "<a href=\"http://xxblx.bitbucket.org/\">Oleg Kozlov (xxblx)</a> | <a href=\"http://forum.mageialinux.ru\" alt=\"MRC forums\">Mageia Russian Community</a> | 2014 - 2015",
    rep_support: "This repository support Mageia ",
    or_newer: " or newer",
    tarakbumba_title: "tarakbumba's repository",
    tarakbumba_desc: "In this <a href=\"http://tarakbumba.mageia.org.pl/\">repository</a> you can find rpm packages which are build and maintained by <a href=\"http://tarakbumba.com/\">tarakbumba</a>.",
    add_btn_tarakbumba: "Add",
    manual_mirror: "Select mirror manually",
    scroll_btn: "Up",
    proxy_label: "Proxy",
    phost_label: "Host(:Port)",
    puser_label: "User",
    ppswd_label: "Password",
    unrealnetworks_title: "UnrealNetworks.co.uk repository",
    unrealnetworks_desc: "Repository of <b>pete910</b> and <b>doctor5000</b> from <a href=\"https://forums.mageia.org\">forums.mageia.org</a>.",
    add_btn_unrealnetworks: "Add",
    pol_title: "Mageia.org.pl repository",
    pol_desc: "Repository of <a href=\"http://mageia.org.pl/\">Polish Mageia Community</a>.",
    add_btn_pol: "Add",
    cyron_title: "Cyron's repository",
    cyron_desc: "<a href=\"http://cyron.cz/\">Cyron</a> is a Czech packager.",
    add_btn_cyron: "Add",
    mol_title: "MageiaLinuxOnline repository",
    mol_desc: "<a href=\"http://www.mageialinux-online.org/\">MageiaLinuxOnline</a> is a French Mageia Community.",
    add_btn_mol: "Add"
};

tr.tr = { // turkish translation
    mga_rel: "Mageia sürümü:",
    mga_arch: "Mimari:",
    of_rep: "Mageia resmi paket depoları",
    def_mirror: "Öntanımlı yansı",
    def_mirror_desc: "Yansı, coğrafi konumunuza göre kendiliğinden seçilecektir.",
    mrc_rep: "MRC Paket Deposu",
    mrc_rep_desc: "Mageia Rusya Topluluğu veri kaynağı.",
    yamrc_rep_desc: "Sunucumuz erişilebilir durumda değilse Yandex yansısını kullanabilirsiniz.",
    other_repos: "Diğer gayri resmi depolar",
    blogdrake_title: "BlogDRAKE paket deposu",
    blogdrake_desc: "<a href=\"http://blogdrake.net/\">BlogDRAKE</a> bir İspanyol topluluğudur.",
    add_btn_blogdrake: "Ekle",
    spoiler_btn: "Saklanmış bilgi", 
    spoiler_btn2: "Saklanmış bilgi", 
    add_btn: "Ekle",
    add_btn_mrc: "Ekle",
    add_btn_yamrc: "Yansı ekle",
    instr: "Yönerge",
    recbr: "Önerilen web tarayıcı - Firefox 20+",
    press_add: "1. Depo bloğundaki Ekle düğmesine tıklayın",
    open: "2. Aç - Urpmi deposu ekle - TAMAM",
    auth: "3. Kök şifrenizi girin - Kimlik doğrulama",
    press_yes: "4. Evet düğmesine tıklayın",
    ocu: "OneClick URPMI GNU GPL v3 veya daha yeni sürümü altında lisanslıdır. Kaynak kodu <a href=\"https://bitbucket.org/xxblx/oneclick-urpmi\">bitbucket.org</a> sitesinde bulunabilir.",
    author: "<a href=\"http://xxblx.bitbucket.org/\">Oleg Kozlov (xxblx)</a> | <a href=\"http://forum.mageialinux.ru\" alt=\"MRC forumları\">Mageia Rusya Topluluğu</a> | 2014 - 2015",
    rep_support: "Bu depo Mageia' yı destekler. ",
    or_newer: " veya daha yenisini",
    tarakbumba_title: "tarakbumba'nın deposu",
    tarakbumba_desc: "Bu <a href=\"http://tarakbumba.mageia.org.pl/\">depoda</a>, <a href=\"http://tarakbumba.com/\">tarakbumba</a> tarafından derlenen ve bakımı yapılan paketleri bulabilirsiniz.",
    add_btn_tarakbumba: "Ekle",
    manual_mirror: "Yansıyı elle ekle",
    scroll_btn: "Yukarı",
    proxy_label: "Vekil Sunucu",
    phost_label: "Makine(:Port)",
    puser_label: "Kullanıcı",
    ppswd_label: "Parola",
    unrealnetworks_title: "UnrealNetworks.co.uk deposu",
    unrealnetworks_desc: "<a href=\"https://forums.mageia.org\">forums.mageia.org</a> adresinden <b>pete910</b> ve <b>doctor5000</b> kullanıcılarının deposu .",
    add_btn_unrealnetworks: "Ekle",
    pol_title: "Mageia.org.pl deposu",
    pol_desc: "<a href=\"http://mageia.org.pl/\">Mageia Polonya Topluluğu</a>nun deposu.",
    add_btn_pol: "Ekle",
    cyron_title: "Cyron' un deposu",
    cyron_desc: "<a href=\"http://cyron.cz/\">Cyron</a> bir Çek paketleyicidir.",
    add_btn_cyron: "Ekle",
    mol_title: "MageiaLinuxOnline deposu",
    mol_desc: "<a href=\"http://www.mageialinux-online.org/\">MageiaLinuxOnline</a> bir Fransız Mageia Topluluğudur.",
    add_btn_mol: "Ekle"
};

tr.pl = { // polish translation
    mga_rel: "Wydanie Mageia:",
    mga_arch: "Architektura:",
    of_rep: "Oficjalne repozytoria Mageia",
    def_mirror: "Domyślny mirror",
    def_mirror_desc: "Mirror zostanie wybrany automatycznie na podstawie twojej geograficznej lokalizacji.",
    mrc_rep: "Repozytorium MRC",
    mrc_rep_desc: "Repozytorium rosyjskiej społeczności Mageia.",
    yamrc_rep_desc: "Jeśli nasz serwer nie jest dostępny możesz użyć mirrora Yandex.",
    other_repos: "Inne nieoficialne repozytoria",
    blogdrake_title: "Repozytorium BlogDRAKE",
    blogdrake_desc: "<a href=\"http://blogdrake.net/\">BlogDRAKE</a> to hiszpańska społeczność",
    add_btn_blogdrake: "Dodaj",
    spoiler_btn: "Pokaż/Ukryj", 
    spoiler_btn2: "Pokaż/Ukryj", 
    add_btn: "Dodaj",
    add_btn_mrc: "Dodaj",
    add_btn_yamrc: "Dodaj mirror",
    instr: "Instrukcja",
    recbr: "Rekomendowana przeglądarka - Firefox 20+",
    press_add: "1. Wciśnij przycisk Dodaj znajdujący się przy danym repozytorium",
    open: "2. Otwórz za pomocą - Dodawanie nośnika dla urpmi - OK",
    auth: "3. Wpisz hasło roota - Uwierzytelnianie",
    press_yes: "4. Wciśnij Tak",
    ocu: "OneClick URPMI jest dostępne na licencji GNU GPL v3 lub nowszej. Kod źródłowy dostępny <a href=\"https://bitbucket.org/xxblx/oneclick-urpmi\">na bitbucket.org</a>.",
    author: "<a href=\"http://xxblx.bitbucket.org/\">Oleg Kozlov (xxblx)</a> | <a href=\"http://forum.mageialinux.ru\" alt=\"MRC forums\">Mageia Russian Community</a> | 2014 - 2015",
    rep_support: "This repository support Mageia ",
    or_newer: " or newer",
    tarakbumba_title: "Repozytorium Tarakbumba",
    tarakbumba_desc: "W tym <a href=\"http://tarakbumba.mageia.org.pl/\">repozytorium</a> znajdziesz pakiety przygotowane przez <a href=\"http://tarakbumba.com/\">tarakbumba</a>.",
    add_btn_tarakbumba: "Dodaj",
    manual_mirror: "Ręczny wybór mirroru",
    scroll_btn: "Do góry",
    proxy_label: "Proxy",
    phost_label: "Host(:Port)",
    puser_label: "Użytkownik",
    ppswd_label: "Hasło",
    unrealnetworks_title: "Repozytorium UnrealNetworks.co.uk",
    unrealnetworks_desc: "Repozytorium tworzone przez <b>pete910</b> oraz <b>doctor5000</b> z <a href=\"https://forums.mageia.org\">forums.mageia.org</a>.",
    add_btn_unrealnetworks: "Dodaj",
    pol_title: "Repozytorium Mageia.org.pl (MOPL)",
    pol_desc: "Repozytorium <a href=\"http://mageia.org.pl/\">polskiej społeczności Mageia</a>.",
    add_btn_pol: "Dodaj",
    cyron_title: "Repozytorium Cyrona",
    cyron_desc: "<a href=\"http://cyron.cz/\">Cyron</a> to czeski paczkowacz.",
    add_btn_cyron: "Dodaj",
    mol_title: "Repozytorium MageiaLinuxOnline",
    mol_desc: "<a href=\"http://www.mageialinux-online.org/\">MageiaLinuxOnline</a> to francuska społeczność Mageia.",
    add_btn_mol: "Dodaj"
};