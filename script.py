#!/usr/bin/python
# -*- coding: utf-8 -*-

# This script used for js/servs.js generation
# You don't need copy this script to web-hosting

from HTMLParser import HTMLParser
from urllib2 import urlopen
from re import findall, sub, DOTALL

class str_parser(HTMLParser):
    """ Get server name/location """
    
    def handle_data(self, data):
        self.res = data

class url_parser(HTMLParser):
    """ Get url """
    
    def handle_starttag(self, tag, attrs):
        
        # Check is self.urls list created
        try:
            if self.urls:
                pass
        except AttributeError:
            self.urls = {}
        
        # Get http/ftp urls
        if tag.lower() == "a":            
            if attrs[0][1][0:4] == "http":
                if attrs[0][1][-1] == "/":
                    self.urls["http"] = attrs[0][1] + "distrib/"
                else:
                    self.urls["http"] = attrs[0][1] + "/distrib/"
            elif attrs[0][1][0:3] == "ftp":
                if attrs[0][1][-1] == "/":
                    self.urls["ftp"] = attrs[0][1] + "distrib/"
                else:
                    self.urls["ftp"] = attrs[0][1] + "/distrib/"

# HTML
html_data = urlopen("http://mirrors.mageia.org/").read()

# List of <td> </td>'s
td_list = findall("<td>.*?</td>", html_data, DOTALL)

serv_names = []
serv_locs = []
serv_urls = {}

i = 1
for count in range(len(td_list)):
    
    # Remove \n from string
    s = sub("\n", "", td_list[count])
    
    # Server name
    if i == 1:
        # Parser
        pr = str_parser()
        pr.feed(s)
        # Result
        serv_names.append(pr.res)
        
    # Server location
    elif i == 2:
        pr = str_parser()
        pr.feed(s)
        serv_locs.append(pr.res)
    
    # URLs
    elif i == 5:
        pr = url_parser()
        pr.feed(s)
        if len(pr.urls) > 0:
            serv_urls[serv_names[-1]] = pr.urls
        
    if i >= 5:
        i = 1
    else:
        i += 1

# Get nums of items without urls or with rsync only
to_rm = []
for i in range(len(serv_names)):
    if not serv_names[i] in serv_urls.keys():
        to_rm.append(i)

# Remove
for i in range(len(to_rm), 0, -1):
    serv_names.__delitem__(to_rm[i - 1])
    serv_locs.__delitem__(to_rm[i - 1])

# Content
js_data = ""
for i in range(len(serv_names)):
    
    urls = []
    rels = []
    
    js_data += "servs[" + str(i) + "] = {name: \"" + serv_names[i] 
    js_data += "\", loc: \"" + serv_locs[i] + "\", urls: {"
    
    if "ftp" in serv_urls[serv_names[i]]:
        js_data += "ftp: \"" + serv_urls[serv_names[i]]["ftp"] + "\", "
        urls.append(serv_urls[serv_names[i]]["ftp"])
    
    if "http" in serv_urls[serv_names[i]]:
        js_data += "http: \"" + serv_urls[serv_names[i]]["http"] + "\", "
        urls.append(serv_urls[serv_names[i]]["http"])
    
    js_data += "}, rels: ["
    
    # Test urls for every release availability
    for lnk in urls:
        for rel in range(1, 5):
            try:
                urlopen(lnk + str(rel))
                rels.append(rel)
            except:
                pass
    
    # Sort and delete duplicates
    for r in list(set(rels)):
        js_data += str(r) + ", "
    
    js_data += "]};\n\n"

# Write to file
with open("js/servs.js", "w") as f:
    
    f.write("// Content of this file automatically genereted by script.py\n\n")
    f.write("var servs = [];  // servers list\n\n")
    f.write(js_data)
