OneClick URPMI
=======================
# Description (English)
**Описание на русском языке см. ниже.**

[OneClick URPMI](http://urpmi.mageialinux.ru) is a small web-tool. It allow to users easy and quick add Mageia repositories (addmedia) into system. 
OneClick URPMI inspired by EasyURPMI for Mandriva. 

Once you need for using OneClick URPMI as user (client) is Firefox 20+ with enabled JavaScript support. 
OneClick URPMI fully work on client-side. So, it can be placed on any free web-hosting.  

Licensed under GNU GPL v3. See LICENSE for details. 

## One Click URPMI use few additional JavaScript libraries: 
- [jQuery](http://jquery.com/) (MIT License)
- [FileSaver.js](https://github.com/eligrey/FileSaver.js) (MIT License) - Used for file generation on client-side

# Описание (Russian)
[OneClick URPMI](http://urpmi.mageialinux.ru) - небольшая веб-утилита, позволяющая пользователям быстро и легко подключить источники (репозитории) для Mageia. 
OneClick URPMI создана по прообразу EasyURPMI для Mandriva. 

Чтобы использовать OneClick URPMI в качестве пользователя (клиента), все что вам нужно - Firefox 20+ с включенной поддержкой JavaScript. 
OneClick URPMI полностью работает на стороне клиента. По этому может быть размещена на любом бесплатном веб-хостинге. 

Распространяется по условиям лицензии GNU GPL v3. См. подробности в LICENSE. 

## One Click URPMI использует несколько дополнительных JavaScript библиотек: 
- [jQuery](http://jquery.com/) (MIT License)
- [FileSaver.js](https://github.com/eligrey/FileSaver.js) (MIT License) - Используется для генерации файлов на стороне клиента

[Mageia Russian Community](http://forum.mageialinux.ru)

xxblx, 2014 - 2015
