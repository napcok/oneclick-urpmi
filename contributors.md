
## Author & Developer
* Oleg Kozlov (xxblx) <xxblx.oleg@yandex.com>

## Translators
* ru - Oleg Kozlov (xxblx)
* tr - Atilla ÖNTAŞ (tarakbumba)
* pl - Daniel Napora (napcok)
